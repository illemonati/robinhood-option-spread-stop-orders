import robin_stocks.robinhood as r
from decimal import Decimal
import getpass
import time


def pretty_print_dict(d):
    for key, value in d.items():
        print(f'{key: <50}', end=' ')
        if type(value) is dict:
            print()
            pretty_print_dict(value)
        else:
            print(value)


def show_account_info():
    info = r.load_account_profile()
    pretty_print_dict(info)


def prompt_login():
    print("Please input your login information")
    username = input("> username: ")
    password = getpass.getpass("> password: ")
    login = r.login(username, password)


def get_spread_price_info(symbol, legs):
    bid = Decimal(0)
    bid_size = 0xFFFFF
    ask = Decimal(0)
    ask_size = 0xFFFFF
    mid = Decimal(0)
    for leg in legs:
        leg_market_data = r.get_option_market_data(
            symbol,
            leg['expirationDate'],
            leg['strike'],
            leg['optionType'],
        )
        data = leg_market_data[0][0]
        if leg['action'] == 'buy':
            bid += Decimal(data['bid_price'])
            bid_size = min(data['bid_size'], bid_size)
            ask += Decimal(data['ask_price'])
            ask_size = min(data['ask_size'], ask_size)
            mid += Decimal(data['adjusted_mark_price'])
        elif leg['action'] == 'sell':
            bid -= Decimal(data['ask_price'])
            bid_size = min(data['ask_size'], bid_size)
            ask -= Decimal(data['bid_price'])
            ask_size = min(data['bid_size'], ask_size)
            mid -= Decimal(data['adjusted_mark_price'])

    if mid < 0:
        bid, ask = -ask, -bid
        mid = -mid

    return {
        'mid': mid,
        'bid': bid,
        'ask': ask,
        'bid_size': bid_size,
        'ask_size': ask_size
    }


def enter_spread_stop_order():
    direction = input("> direction (c/d): ")
    dm = {
        'c': 'credit',
        'd': 'debit',
    }
    if direction not in dm:
        raise Exception('Invalid direction')
    direction = dm[direction]

    stop_price = Decimal(input("> stop price: "))
    limit_price = ''
    limit_or_market = input("> limit or market (limit/market): ")
    if limit_or_market == 'limit':
        limit_price = input("> limit price: ")
    symbol = input("> symbol: ")
    quantity = input("> quantity: ")
    legs = []
    leg = 0
    while True:
        add_leg = input("> add leg? (y/N): ")
        add_leg = add_leg == 'y'
        if not add_leg:
            break
        leg += 1
        expiry = input(f"> [leg {leg}] expiry (YYYY-MM-DD): ")
        strike = input(f"> [leg {leg}] strike: ")
        option_type = input(f"> [leg {leg}] option type (call/put): ")
        effect = input(f"> [leg {leg}] effect (open/close): ")
        action = input(f"> [leg {leg}] action (buy/sell): ")
        legs.append({
            'expirationDate': expiry,
            'strike': strike,
            'optionType': option_type,
            'effect': effect,
            'action': action
        })

    while True:
        price_info = get_spread_price_info(symbol, legs)
        pretty_print_dict(price_info)
        if direction == 'debit' and price_info['mid'] < stop_price or direction == 'credit' and price_info['mid'] > stop_price:
            time.sleep(1)
            continue
        result = r.order_option_spread(
            direction,
            limit_price if limit_or_market == 'limit' else
            str(price_info['ask']) if direction == 'debit' else
            str(price_info['bid']),
            symbol,
            quantity,
            spread=legs,
            timeInForce='gtc'
        )

        pretty_print_dict(result)
        return


menu_options = [
    {
        'prompt': "Show account information",
        'function': show_account_info
    },
    {
        'prompt': "Enter spread stop order",
        'function': enter_spread_stop_order
    }
]


def main():
    try:
        prompt_login()
    except Exception as e:
        print(e)
        exit()

    while True:
        print("Please select an option")
        for i, option in enumerate(menu_options):
            print(f'{i+1}) {option["prompt"]}')
        try:
            option_selected = int(input('> '))
            if option_selected < 1 or option_selected > len(menu_options):
                raise Exception('Invalid option')

            func = menu_options[option_selected-1]['function']
            if func is not None:
                try:
                    func()
                except Exception as e:
                    print(e)

        except Exception as e:
            print(e)
            print('Invalid input, please try again')


if __name__ == '__main__':
    main()
